/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rtspclient;

import gui.GUI;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.opencv.core.Core;

/**
 *
 * @author kevin
 */
public class RTSPClient extends javax.swing.JFrame {
    
    // load opencv
    static {
        String path = null;
        try {
            //I have copied dlls from opencv folder to my project folder
            path = "C:\\Projects\\RTSPClient";
            System.load(path + "\\opencv_java412.dll");
            System.load(path + "\\opencv_videoio_ffmpeg412_64.dll");
        } catch (UnsatisfiedLinkError e) {
            System.out.println("Error loading libs");
        }
    }
    
    public RTSPClient() {
        
    }
    
    private void paint(RTSPClient rtspClient) {
        
         /** MAIN PANEL */
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout( new FlowLayout() );
        mainPanel.add( GUI.getMainViewGUI() );
        
        /** CONTROL PANEL */
        JPanel controlPanel = new JPanel();
        controlPanel.setLayout( new FlowLayout() );
        controlPanel.add( GUI.getControlGUI() );
        
        /** Main Frame */
        this.setLayout( new FlowLayout() );
        this.add( mainPanel );
        this.add( controlPanel );
        this.setMinimumSize( new Dimension( 990, 680 ) );
        
        rtspClient.setTitle( "RTSP Streams" );
        rtspClient.pack();
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        // center the frame in the physical screen
        rtspClient.setLocation( ( dimension.width - rtspClient.getWidth() ) / 2, ( dimension.height - rtspClient.getHeight() ) / 2 );
        rtspClient.setVisible( true );
        this.setDefaultCloseOperation( System.getProperty( "javawebstart.version" ) != null ? JFrame.EXIT_ON_CLOSE : JFrame.DISPOSE_ON_CLOSE );
        this.addWindowListener( new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing( java.awt.event.WindowEvent windowEvent ) {
                // reset timers and finish all active projects
                GUI.getMainViewGUI().stopCameras();
            }
        });
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {

        // load opencv
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        
        SwingUtilities.invokeLater( new Runnable() {

        @Override
	public void run() {

                JFrame.setDefaultLookAndFeelDecorated( true );
                JDialog.setDefaultLookAndFeelDecorated( true );

                RTSPClient rtspClient = new RTSPClient();
                rtspClient.addComponentListener( new ComponentAdapter() {
                    @Override
                    public void componentResized( ComponentEvent e ) {
                        super.componentResized( e );
                        ( ( JFrame ) e.getComponent() ).getRootPane().repaint();
                    }
                });

                rtspClient.paint( rtspClient );

        }

        });

    }
    
}
