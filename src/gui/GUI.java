/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.JFrame;

/**
 *
 * @author kevin
 */
public class GUI extends JFrame {
    
    private static MainViewGUI mainViewGUI = null;
    private static ControlGUI controlGUI = null;

    public GUI() {

        super( "RTSP" );

    }
    
    public static MainViewGUI getMainViewGUI() {

        if( mainViewGUI == null ) {
            mainViewGUI = new MainViewGUI();
            mainViewGUI.paint();
        }

        return ( mainViewGUI );
        
    }
    
    public static ControlGUI getControlGUI() {
        
        if( controlGUI == null ) {
            controlGUI = new ControlGUI();
            controlGUI.paint();
        }
        
        return ( controlGUI );
        
    }
    
}
