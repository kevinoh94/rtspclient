/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

/**
 *
 * @author kevin
 */
public class CameraViewPanel extends JPanel {
    
    private BufferedImage imageToShow;
    private BufferedImage image;
    private Graphics2D g2d;
    private final String address;
    
    // a timer for acquiring the video stream
    private ScheduledExecutorService timer;
    // the OpenCV object that realizes the video capture
    private VideoCapture capture = new VideoCapture();
    private Mat frame = new Mat();
    private boolean cameraActive = false;
    
    private byte[] b = new byte[3 * 1920 * 1080]; // # of channels * length * width
    private byte[] targetPixels;
    private static int type = BufferedImage.TYPE_3BYTE_BGR;
    private CameraWorker cameraWorker;
    
    
    public CameraViewPanel( String cameraAddress ) {
        
        address = cameraAddress;
        setDefaultImage();
        
    }
    
    @Override
    protected void paintComponent( Graphics g ) {

        super.paintComponent( g );

        g2d = ( Graphics2D )g;
        
        updateImageView(imageToShow);
        
    }
    
    private void setDefaultImage() {
        
        String IMG_PATH = "disconnected.jpg";
        try {
            imageToShow = ImageIO.read(new File(IMG_PATH));
        } catch (IOException ex) {
            Logger.getLogger(CameraViewPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public boolean playCamera() {
        
        if (!cameraActive) {
            // start the video capture
            capture.open(address);

            // is the video stream available?
            if (capture.isOpened()) {
                cameraActive = true;
                cameraWorker = new CameraWorker();
                cameraWorker.execute();
                // grab a frame every 33 ms (30 frames/sec)
//                this.timer = Executors.newSingleThreadScheduledExecutor();
//                this.timer.scheduleAtFixedRate(new CameraWorker(), 0, 33, TimeUnit.MILLISECONDS);
                
            } else {
                System.err.println("Impossible to open the camera connection...");
            }
        } else {
            this.stopAcquisition();
        }
        
        return cameraActive;
        
    }
        
    class CameraWorker extends SwingWorker<Void, BufferedImage> {
        
        @Override
        protected Void doInBackground() throws Exception {
            
            while(!isCancelled()) {
                imageToShow = toBufferedImage( grabFrame(capture) );
                Thread.sleep(33);
                publish(imageToShow);
                
            }
            return null;
        }
        
        @Override
        protected void process(List<BufferedImage> images) {
            BufferedImage image = images.get(images.size() - 1);
            updateImageView(image);
            repaint();
        }
    }
        
    private Mat grabFrame(VideoCapture capture) {
        
        if (capture.isOpened()) {
            try {
                capture.read(frame);
            } catch (Exception e) {
                System.err.println("Exception during the image elaboration: " + e);
            }
        }

        return frame;
        
    }
    
    private void updateImageView(BufferedImage imageToShow) {
        
        g2d.drawImage(imageToShow, 0, 0, 375, 300, this );
        
    }
    
    private BufferedImage toBufferedImage(Mat m) {
        
        /*
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        */
        
        if(b == null) {
            // Initialize object once and reuse
            initializeB(m);
        }
        if(image == null) {
            initializeImage(m);
        }
        
        m.get(0, 0, b); // get all the pixels
        targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;

    }
    
    private void initializeB(Mat m) {
        
        int bufferSize = m.channels() * m.cols() * m.rows();
        b = new byte[bufferSize];
        
    }
    
    private void initializeImage(Mat m) {
        
        image = new BufferedImage(m.cols(), m.rows(), type);
        
    }
    
    public void stopAcquisition() {
        
        cameraActive = false;
        try {
            if(cameraWorker != null) {
                cameraWorker.cancel(cameraActive);
                Thread.sleep(300);
            }
        } catch (InterruptedException e) {
            System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
        }
        
//        if (this.timer != null && !this.timer.isShutdown()) {
//            try {
//                // stop the timer
//                this.timer.shutdown();
//                this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
//                
//            } catch (InterruptedException e) {
//                // log any exception
//                System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
//            }
//        }

        if (this.capture.isOpened()) {
            capture.release();
        }
        setDefaultImage();
        updateImageView(imageToShow);
        repaint();
        
    }
}
