/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author kevin
 */
public class MainViewGUI extends JPanel {
    
    private List<String> cameraAddresses;
    private List<CameraViewPanel> cameraViewPanels;
    
    public MainViewGUI() {
        
        cameraAddresses = new ArrayList<>();
        cameraViewPanels = new ArrayList<>();
        
    }
    
    public void paint() {
        
        getCameras();
        
        GridLayout gridLayout = new GridLayout( 0,2 );
        gridLayout.setHgap( 5 );
        gridLayout.setVgap( 10 );
        JPanel panel = new JPanel( gridLayout );
        JScrollPane scrollPane = new JScrollPane(panel);
        
        for (int i = 0; i < cameraAddresses.size(); i++) {
            CameraViewPanel cameraViewPanel = new CameraViewPanel(cameraAddresses.get(i));
            cameraViewPanels.add(cameraViewPanel);
            panel.add(cameraViewPanel);
        }


        panel.setPreferredSize( new Dimension( 760, 680 ) );
        panel.setVisible(true);
        scrollPane.setPreferredSize( new Dimension( 760, 680 ) );
        
        this.setLayout( new GridBagLayout() );
        GridBagConstraints gbC = new GridBagConstraints();
        gbC.fill = GridBagConstraints.BOTH;
        gbC.gridy = 0;
        
        this.add( scrollPane, gbC );
        
        this.setBorder( BorderFactory.createRaisedBevelBorder() );
        
    }
    
    private void getCameras() {
        
        cameraAddresses.add( "rtsp://admin:88888888@192.168.0.10:10554/udp/av0_0" );
        cameraAddresses.add( "rtsp://admin:88888888@192.168.0.12:10554/udp/av0_0" );
        cameraAddresses.add( "rtsp://admin:88888888@192.168.0.14:10554/udp/av0_0" );
        cameraAddresses.add( "rtsp://admin:88888888@192.168.0.16:10554/udp/av0_0" );
        
    }
    
    public boolean playCameras() {
        
        boolean cameraActive = false;
        for(int i = 0; i < cameraViewPanels.size(); i++ ) {
            cameraActive = cameraViewPanels.get(i).playCamera();
        }
        return cameraActive;
        
    }
    
    public void stopCameras() {
        
        for(int i = 0; i < cameraViewPanels.size(); i++ ) {
            cameraViewPanels.get(i).stopAcquisition();
        }
        
    }
    
}
