/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author kevin
 */
public class ControlGUI extends JPanel {
    
    public ControlGUI() {
        
    }
    
    public void paint() {
        
        JPanel panel = new JPanel( new FlowLayout() );
        JButton playButton = new JButton( "Start" );
        
        playButton.addActionListener((ActionEvent e) -> {
            if( GUI.getMainViewGUI().playCameras() ) {
                playButton.setText( "Stop" );
            } else {
                playButton.setText( "Start" );
            }
        });
        
        panel.add( playButton );
        panel.setPreferredSize( new Dimension( 180, 50 ) );
        panel.setVisible(true);
        
        this.setLayout( new GridBagLayout() );
        GridBagConstraints gbC = new GridBagConstraints();
        gbC.fill = GridBagConstraints.BOTH;
        gbC.gridy = 0;
        
        this.add( panel, gbC );
        this.setBackground(Color.DARK_GRAY);
        
        this.setBorder( BorderFactory.createRaisedBevelBorder() );
        
    }
    
}
