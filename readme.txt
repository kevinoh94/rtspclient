1. 	In RTSPClient.java must set the path to where OpenCV library files are located:
	"opencv_java###.dll" and
	"opencv_videoio_ffmpeg###_64.dll"
2. 	In MainViewGUI.java add camera IP addresses to cameraAddresses list in getCameras() method to add panels
for camera views.  
	Use ip finder application from here:
		https://www.vstarcam.com/software-download
	to get ip addresses of cameras. You need to include user id, password, and port in the url.
	Example url: rtsp://admin:password@192.168.0.10:10554/udp/av0_0
	(Option) From the ip finder application you can connect to the camera through your browser. From the browser you can set the RTSP port number. 